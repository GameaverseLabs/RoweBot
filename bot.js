/*   Notes for us
*
*   Class Names:  Uppercase:Uppercase (CommandParser)
*   Variables:    lowercase:Uppercase (someVariable)
*
*/

// All the requirements
const Discord = require('discord.js')
const Client = new Discord.Client()
const fs = require('fs')
const ConfigHandler = require('./src/util/ConfigHandler')
const Logger = require('./src/util/Logger')
const CommandLoader = require('./src/util/CommandLoader')
const CommandParser = require('./src/util/CommandParser')
const SQLDriver = require('./src/util/SQLDriver')
const Model = require('./src/util/ModelDriver').Model;

// lol xd?

// variables
var config = process.env; // Will be changed to a file handled by the config handler
var commandLoader = new CommandLoader('./commands');
var dataDB = new SQLDriver('./data/data.sqlite');

// models
const user = new Model(dataDB, {
    name: "user",
    values: ["id INTEGER PRIMARY KEY",
             "userId STRING",
             "coins INTEGER",
             "xp INTEGER"]
});

const guild = new Model(dataDB, {
    name: "guild",
    values: ["id INTEGER PRIMARY KEY",
             "guildId STRING",
             "prefix STRING"]
});

console.log("Is this thing on?");

// just in case we need it
module.exports.commands = commandLoader.commands;

// Event Handlers
Client.on('ready', () => {
  console.log(`Logged in as ${Client.user.tag}!`);
})

Client.on('guildCreate', function (guild) {
    // dataDB.schemas.guild.Create({
    //     guildId: guild.id,
    //     guildName: guild.name,
    //     prefix: '!'
    // });
    dataDB.createObject("guild", {
        guildId: guild.id,
        prefix: '!'
    });
});

Client.on('message', async function(msg) {
    console.log(msg.content);

    if (await dataDB.get("user", "userId", msg.author.id) == null) {
        await dataDB.createObject("user", {
            userId: msg.author.id,
            coins: 10,
            xp: 0
        });
    }

    CommandParser.parseCommand(msg);

    var commandInvoker = '!' // tmep
    var cmd = msg.content.split(' ')
                         .slice(0, 1)
                         .join('')
                         .replace(commandInvoker, '');

    var args = msg.content.split(' ')
                          .slice(1);
})

Client.login(config.token);
