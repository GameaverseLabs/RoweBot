const sqlite3 = require("sqlite3");
const createStatus = require("./Status").createStatus;
const Logger = require("./Logger");

class SQLDriver {
    constructor(filename) {
        this.dbPromise = new sqlite3.Database(filename);
    }

    async createTable(name, values) {
        var db = await getDB();

        db.run(`CREATE TABLE ${name} (${values.join(", ")})`);
    }

    async insert(table, columns, values) {
        var db = await getDB();

        db.run(`INSERT INTO ${table} (${columns.join(", ")}) VALUES (${values.join(", ")})`);
    }
}

async function getDB() {
    var db = await this.dbPromise;

    if (!db) {
        Logger.error("Could not open database.");
        return createStatus(-1, "Could not open database.");
    }
}

module.exports = SQLDriver;
