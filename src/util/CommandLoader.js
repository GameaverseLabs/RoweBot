const fs = require("fs");
const createStatus = require("./Status").createStatus;
const Logger = require("./Logger");
const utils = require("./Util")
const path = require("path");
const Discord = require("discord.js");

// const index_dir = "/Users/owen/Projects/Discord Bots/Rowe Bot 2/";

class CommandLoader {
    constructor(dir) {
        this.dir = dir;
        this.commands = new Discord.Collection();

        this.load();
    }

    load() {

        var dir = this.dir;
        var commands = this.commands;
        // if (!utils.isDirSync(this._dir)) {
        //     Logger.error("Provided directory does not exist.")
        //     Logger.info(this._dir);
        //     return createStatus(-1, "Provided directory does not exist.");
        // }

        fs.readdirSync(this.dir).forEach(function(command){
            try {
                var commandName = command;
                var command = require(path.join("../../commands", command));
            }
            catch(e) {
                Logger.error(`Error loading command, '${command}'.`);
                Logger.error(e);
                return createStatus(-1, `Error loading command, '${command}'.`);
            }

            try {
                command = new command();
            }
            catch(e) {
                Logger.error(`Error initializing command, '${commandName}'.`)
                return createStatus(-1, `Error initializing command, '${commandName}'.`);
            }

            if (!command) return;

            commands.set(command.name, command);
        });
    }
}

module.exports = CommandLoader;