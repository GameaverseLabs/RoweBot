class Status {
    constructor(id, msg, time) {
        this._id = id;
        this._msg = msg;
        this._time = time;
    }

    toString() {
        return `${_id}: ${_msg}`
    }

    toInt() {
        return _id
    }

    toBoolean() {
        if (_id < 0) {
            return false;
        }
        else {
            return true;
        }
    }

    toJSON() {
        return {id: _id,
                message: _msg,
                time: _time};
    }
}

module.exports = Status;

module.exports.createStatus = function(id, msg) {
    return new Status(id, msg, Date.now());
}