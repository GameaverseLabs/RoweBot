const createStatus = require("./Status").createStatus;
const Logger = require("./Logger");

class Model {
    constructor(db, data) {
        this.db = db;

        if (!data.name || !data.values) {
            Logger.error("Couldn't initialize model. Name and/or values are missing.");
            return createStatus(-1, "Couldn't initialize model. Name and/or values are missing.");
        }

        this.name = data.name;
        this.values = data.values;

        db.createTable(data.name, data.values);
    }

    async create(){
        
    }
}

module.exports = {
    Model
}

//Ill be working on commands okay?